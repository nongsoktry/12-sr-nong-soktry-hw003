package homework;


public class Volunteer extends StaffMember {
    public Volunteer(int id, String name, String address) {
        super(id, name, address);
    }

    @Override
    public String toString() {
        return "Employee ID: " + id + "\nEmployee Name: " + name + "\nEmployee Address: " + address;
    }

    @Override
    public double pay() {
        return 0;
    }

}