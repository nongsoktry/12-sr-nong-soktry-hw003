package homework;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    static int employeeID;
    static String employeeName;
    static String employeeAddress;
    static ArrayList<StaffMember> arrayList = new ArrayList<>();
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        while (true) {
            displayAllEmployee(arrayList);
            chooseOption();
        }
    }

    private static void chooseOption() {
        System.out.println("\n========== Employee Management ==========");
        System.out.println("1). Add Employee");
        System.out.println("2). Edit Employee");
        System.out.println("3). Remove Employee");
        System.out.println("4). Exit");
        System.out.println("-----------------------------------------");
        System.out.print("==> Choose option(1-4): ");
        int optionToSelect;
        do {
            Scanner scnOption = new Scanner(System.in);
            while (!scnOption.hasNextInt()) {
                System.out.println("\tPlease choose option(1-4) only!\n");
                System.out.print("==> Choose option(1-4): ");
                scnOption.next(); // this is important!
            }
            optionToSelect = scnOption.nextInt();
        } while (optionToSelect <= 0);
        switch (optionToSelect) {
            case 1:
                addEmployee();
                break;
            case 2:
                editEmployee();
                break;
            case 3:
                removeEmployee();
                break;
            case 4:
                System.out.println("(^-^) Good Bye! (^-^)");
                System.exit(0);
            default:
                System.out.println("\tPlease choose option(1-4) only!");
                break;
        }
    }

    private static void displayAllEmployee(ArrayList arrayList) {
        System.out.println("\n=========== Display Employee ============");
        if (arrayList.size() == 0) {
            System.out.println("\t\tPlease Create Employee!");
        } else {
            for (Object o : arrayList) {
                System.out.println("-----------------------------------------");
                System.out.println(o);
                System.out.println("-----------------------------------------");
            }
        }
    }

    private static void addEmployee() {
        int typeEmployee;
        Scanner scnEmployee = new Scanner(System.in);

        System.out.println("\n============= Employee Type =============");
        System.out.println("1. Volunteer");
        System.out.println("2. Salary Employee");
        System.out.println("3. Hourly Employee");
        System.out.println("4. Go Back!");
        System.out.println("-----------------------------------------");
        System.out.print("==> Choose option(1-4): ");
        do {
            while (!scnEmployee.hasNextInt()) {
                System.out.println("\tPlease choose option(1-4) only!\n");
                System.out.print("==> Choose option(1-4): ");
                scnEmployee.next(); // this is important!
            }
            typeEmployee = scnEmployee.nextInt();
        } while (typeEmployee <= 0);

        switch (typeEmployee) {
            case 1:
                addVolunteer();
                break;
            case 2:
                addSalariedEmployee();
                break;
            case 3:
                addHourlyEmployeee();
                break;
            case 4:
                chooseOption();
                break;
            default:
                System.out.println("\tPlease choose option(1-4) only!");
                addEmployee();
                break;
        }
    }

    private static void addEmployeeInfo() {
        System.out.println("\n=========== Insert Information ==========");
        System.out.print("=> Enter Employee's ID: ");
        do {
            while (!scanner.hasNextInt()) {
                System.out.println("=> Please enter an ID(number only)!\n");
                System.out.print("=> Enter Employee's ID: ");
                scanner.next();
            }
            employeeID = scanner.nextInt();
        } while (employeeID <= 0);


        System.out.print("=> Enter Employee's Name: ");
        employeeName = scanner.next();

        System.out.print("=> Enter Employee's Address: ");
        employeeAddress = scanner.next();
    }

    private static void addVolunteer() {
        addEmployeeInfo();

        arrayList.add(new Volunteer(employeeID, employeeName, employeeAddress));
        System.out.println("Employee is added successfully!");
        System.out.println();
        promptEnterKey();
    }

    private static void addSalariedEmployee() {
        double employeeSalary;
        double employeeBonus;
        addEmployeeInfo();

        System.out.print("=> Enter Employee's Salary: ");
        do {
            while (!scanner.hasNextDouble()) {
                System.out.println("=> Please Enter Employee's Salary(number only)!\n");
                System.out.print("=> Enter Employee's Salary: ");
                scanner.next();
            }
            employeeSalary = scanner.nextDouble();
        } while (employeeSalary <= 0);

        System.out.print("=> Enter Employee's Bonus: ");
        do {
            while (!scanner.hasNextDouble()) {
                System.out.println("=> Please Enter Employee's Bonus(number only)!\n");
                System.out.print("=> Enter Employee's Bonus: ");
                scanner.next();
            }
            employeeBonus = scanner.nextDouble();
        } while (employeeBonus <= 0);

        arrayList.add(new SalariedEmployee(employeeID, employeeName, employeeAddress, employeeSalary, employeeBonus));

        System.out.println("Employee is added successfully!");
        System.out.println();
        promptEnterKey();
    }

    private static void addHourlyEmployeee() {
        int employeeHoursWorked;
        double employeeRate;
        addEmployeeInfo();

        System.out.print("=> Enter Employee's Hours Worked: ");
        do {
            while (!scanner.hasNextInt()) {
                System.out.println("=> Please Enter Employee's Hours Worked(number only)!\n");
                System.out.print("=> Enter Employee's Hours Worked: ");
                scanner.next();
            }
            employeeHoursWorked = scanner.nextInt();
        } while (employeeHoursWorked <= 0);

        System.out.print("=> Enter Employee's Rate: ");
        do {
            while (!scanner.hasNextInt()) {
                System.out.println("=> Please Enter Employee's Rate(number only)!\n");
                System.out.print("=> Enter Employee's Rate: ");
                scanner.next();
            }
            employeeRate = scanner.nextInt();
        } while (employeeRate <= 0);

        arrayList.add(new HourlyEmployee(employeeID, employeeName, employeeAddress, employeeHoursWorked, employeeRate));

        System.out.println("Employee is added successfully!");
        System.out.println();
        promptEnterKey();
    }

    private static void editEmployee() {
        int employeeEditID;
        System.out.println("\n============= Edit Employee ============");
        System.out.print("=> Enter Employee's ID: ");
        do {
            while (!scanner.hasNextInt()) {
                System.out.println("=> Please enter an ID(number only)!\n");
                System.out.print("=> Enter Employee's ID: ");
                scanner.next();
            }
            employeeEditID = scanner.nextInt();
        } while (employeeEditID <= 0);
        boolean checkID = false;
        for (int i = 0; i < arrayList.size(); i++) {
            if (arrayList.get(i).getId() == employeeEditID) {
                checkID = true;
                break;
            }
        }
        if (!checkID) {
            System.out.println("== There is no employee with this ID!");
        }
        for (StaffMember staffMember : arrayList) {
            if (checkID) {
                if (staffMember.getId() == employeeEditID) {
                    if (staffMember instanceof Volunteer) {
                        System.out.print("=> Enter Employee's Name: ");
                        employeeName = scanner.next();
                        System.out.print("=> Enter Employee's Address: ");
                        employeeAddress = scanner.next();
                        staffMember.setName(employeeName);
                        staffMember.setAddress(employeeAddress);
                        System.out.println("Employee is edited successfully!");
                        break;
                    } else if (staffMember instanceof SalariedEmployee) {
                        System.out.print("=> Enter Employee's Name: ");
                        String editName = scanner.next();
                        System.out.print("=> Enter Employee's Address: ");
                        String editAddress = scanner.next();
                        System.out.print("=> Enter Employee's Salary: ");
                        int editSalary = 0;
                        int editBonus = 0;
                        do {
                            while (!scanner.hasNextInt()) {
                                System.out.println("=> Please Enter Employee's Salary(number only)!\n");
                                System.out.print("=> Enter Employee's Salary: ");
                                scanner.next();
                            }
                            editSalary = scanner.nextInt();
                        } while (editSalary <= 0);

                        System.out.print("=> Enter Employee's Bonus: ");
                        do {
                            while (!scanner.hasNextInt()) {
                                System.out.println("=> Please Enter Employee's Bonus(number only)!\n");
                                System.out.print("=> Enter Employee's Bonus: ");
                                scanner.next();
                            }
                            editBonus = scanner.nextInt();
                        } while (editBonus <= 0);
                        staffMember.setName(editName);
                        staffMember.setAddress(editAddress);
                        ((SalariedEmployee) staffMember).setSalary(editSalary);
                        ((SalariedEmployee) staffMember).setBonus(editBonus);
                        System.out.println("Employee is edited successfully!");
                        break;
                    } else if (staffMember instanceof HourlyEmployee) {
                        System.out.print("=> Enter Employee's Name: ");
                        String editName = scanner.next();
                        System.out.print("=> Enter Employee's Address: ");
                        String editAddress = scanner.next();
                        System.out.print("=> Enter Employee's Hours Worked: ");
                        int editHours = 0;
                        int editRate = 0;
                        do {
                            while (!scanner.hasNextInt()) {
                                System.out.println("=> Please Enter Employee's Hours Worked(number only)!\n");
                                System.out.print("=> Enter Employee's Hours Worked: ");
                                scanner.next();
                            }
                            editHours = scanner.nextInt();
                        } while (editHours <= 0);

                        System.out.print("=> Enter Employee's Rate: ");
                        do {
                            while (!scanner.hasNextInt()) {
                                System.out.println("=> Please Enter Employee's Rate(number only)!\n");
                                System.out.print("=> Enter Employee's Rate: ");
                                scanner.next();
                            }
                            editRate = scanner.nextInt();
                        } while (editRate <= 0);
                        staffMember.setName(editName);
                        staffMember.setAddress(editAddress);
                        ((HourlyEmployee) staffMember).setHoursWorked(editHours);
                        ((HourlyEmployee) staffMember).setRate(editRate);
                        System.out.println("Employee is edited successfully!");
                        break;
                    }
                }
            }
        }
        promptEnterKey();
    }

    private static void removeEmployee() {
        int employeeRemoveID;
        System.out.println("\n============ Remove Employee ===========");
        System.out.print("=> Enter Employee's ID: ");
        do {
            while (!scanner.hasNextInt()) {
                System.out.println("=> Please enter an ID(number only)!\n");
                System.out.print("=> Enter Employee's ID: ");
                scanner.next();
            }
            employeeRemoveID = scanner.nextInt();
        } while (employeeRemoveID <= 0);
        boolean checkID = false;
        for (int i = 0; i < arrayList.size(); i++) {
            if (arrayList.get(i).getId() == employeeRemoveID) {
                checkID = true;
                break;
            }
        }
        if (!checkID) {
            System.out.println("== There is no employee with this ID!");
        }
        for (StaffMember staffMember : arrayList) {
            if (checkID) {
                if (staffMember.getId() == employeeRemoveID) {
                    for (int i = 0; i < arrayList.size(); i++) {
                        if (arrayList.get(i).getId() == employeeRemoveID) {
                            arrayList.remove(i);
                            System.out.println("Employee is removed successfully!");
                            break;
                        }
                    }
                    break;
                }
            }
        }
        promptEnterKey();
    }

    private static void promptEnterKey() {
        System.out.println();
        System.out.println("Press \"ENTER\" to continue...");
        Scanner scn = new Scanner(System.in);
        scn.nextLine();
    }

}